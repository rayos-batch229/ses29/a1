db.courses.insertMany([

    {
        name: "HTML Basics",
        price: 20000,
        isActive: true,
        instructor: "Sir Rome"
    },
    {   
        name: "CSS 101 + Flexbox",
        price: 21000,
        isActive: true,
        instructor: "Sir Rome"
    },
    {   
        name: "Javascript 101",
        price: 32000,
        isActive: true,
        instructor: "Ma'am Tine"
    },
    {   
        name: "Git 101, IDE and CLI",
        price: 19000,
        isActive: false,
        instructor: "Ma'am Tine"
    },
    {   
        name: "React.Js 101",
        price: 25000,
        isActive: true,
        instructor: "Ma'am Miah"
    }


])

// Activity Code

// 1.
db.courses.find({
    
    $or: [
    
        {name:{$regex:'Sir Rome',$options:'$i'}},
        {price:{$gte:20000}}
    ]
    
})

// 2.
db.courses.find({instructor:"Ma'am Tine"},{_id:0,email:0,name:0,isActive:0})
{
    "price" : 32000.0,
    "instructor" : "Ma'am Tine"
}

// 3.
db.courses.find({instructor:"Ma'am Miah"},{_id:0,email:0,name:0,price:0,isActive:0})
{
    "instructor" : "Ma'am Miah"
}

// 4.
   db.courses.find({
   
   $or: [
    
    {courses:{$regex:'r',$options:'$i'}},
    {price:{$gte:20000}}
    ]
})
   /* 1 */
{
    "_id" : ObjectId("63bfc8f50285d14d2e3664a3"),
    "name" : "HTML Basics",
    "price" : 20000.0,
    "isActive" : true,
    "instructor" : "Sir Rome"
}

/* 2 */
{
    "_id" : ObjectId("63bfc8f50285d14d2e3664a5"),
    "name" : "Javascript 101",
    "price" : 32000.0,
    "isActive" : true,
    "instructor" : "Ma'am Tine"
}

/* 3 */
{
    "_id" : ObjectId("63bfc8f50285d14d2e3664a7"),
    "name" : "React.Js 101",
    "price" : 25000.0,
    "isActive" : true,
    "instructor" : "Ma'am Miah"
}

/* 4 */
{
    "_id" : ObjectId("63bfc8f50285d14d2e3664a4"),
    "name" : "CSS 101 + Flexbox",
    "price" : 21000.0,
    "isActive" : true,
    "instructor" : "Sir Rome"
}

//   5.

     db.courses.find({
   
   $and: [
    
    {isActive:true},
    {price:{$lte:25000}}
    ]
})

{
    "_id" : ObjectId("63bfc8f50285d14d2e3664a5"),
    "name" : "Javascript 101",
    "price" : 32000.0,
    "isActive" : true,
    "instructor" : "Ma'am Tine"
}

/* 3 */
{
    "_id" : ObjectId("63bfc8f50285d14d2e3664a7"),
    "name" : "React.Js 101",
    "price" : 25000.0,
    "isActive" : true,
    "instructor" : "Ma'am Miah"
}

/* 4 */
{
    "_id" : ObjectId("63bfc8f50285d14d2e3664a4"),
    "name" : "CSS 101 + Flexbox",
    "price" : 21000.0,
    "isActive" : true,
    "instructor" : "Sir Rome"
}


